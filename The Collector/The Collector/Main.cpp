#include <SFML/Graphics.hpp> // this is the SFML package 
#include <SFML/Audio.hpp> // this load in the sound buffers
#include <string>
#include <vector>
#include "Item.h" // this load in the Item.h
#include "Player.h"
#include <cstdlib> // we are using this to gen random Numbers
#include <time.h> // see above
int main()
{
	
	sf::RenderWindow gameWindow; // this code is seting the screen Var 
	gameWindow.create(sf::VideoMode::getDesktopMode(), "The Collector", sf::Style::Titlebar | sf::Style::Close); // this is setting size of the screen and nameing it 
	
	// -----------------------------------------------
    // Game Setup
	// -----------------------------------------------
	// Player
    // Declare a texture variable called playerTexture
	sf::Texture playerTexture;
	// Load up our texture from a file path
	playerTexture.loadFromFile("Assets/Graphics/player2.png");
	// Declare a player object
	Player playerObject(playerTexture, gameWindow.getSize());
	//-------------------- Seting up Random Number
	srand(time(NULL)); // Clearing Number 
	//-------------- Sounds Game music
	sf::Music gameMusic;
	gameMusic.openFromFile("Assets/Audio/music.ogg");
	gameMusic.play();
	// ------------- Game Text 
	sf::Font gameFont;
	gameFont.loadFromFile("Assets/Fonts/mainFont.ttf");
	float speed;
	
	// Title Text 
	sf::Text titleText;
	titleText.setFont(gameFont);
	titleText.setString("The Collector");
	titleText.setCharacterSize(24);
	titleText.setFillColor(sf::Color::Cyan);
	titleText.setStyle(sf::Text::Bold | sf::Text::Italic);
	titleText.setPosition(gameWindow.getSize().x / 2 - titleText.getLocalBounds().width / 2, 30);

	// Author Text
	sf::Text authorText;
	authorText.setFont(gameFont);
	authorText.setString("by Scott Gilbert");
	authorText.setCharacterSize(16);
	authorText.setFillColor(sf::Color::Magenta);
	authorText.setStyle(sf::Text::Italic);
	authorText.setPosition(gameWindow.getSize().x / 2 - authorText.getLocalBounds().width / 2, 60);


	// Score Text 
	int score = 0;
	sf::Text scoreText;
	scoreText.setFont(gameFont);
	scoreText.setString("Score: " + std::to_string(score));
	scoreText.setCharacterSize(16);
	scoreText.setFillColor(sf::Color::White);
	scoreText.setPosition(30, 30);

	// Timer Text 
	sf::Text timerText;
	timerText.setFont(gameFont);
	timerText.setString("Time Remaining: 0");
	timerText.setCharacterSize(16);
	timerText.setFillColor(sf::Color::White);
	timerText.setPosition(gameWindow.getSize().x - timerText.getLocalBounds().width - 30, 30);
	sf::Time timeLimit = sf::seconds(60.0f);
	sf::Time timeRemaining = timeLimit;
	sf::Clock gameClock;

	// Items
	
// Load all three textures that will be used by our items
	std::vector<sf::Texture> itemTextures;
	itemTextures.push_back(sf::Texture());
	itemTextures.push_back(sf::Texture());
	itemTextures.push_back(sf::Texture());
	itemTextures[0].loadFromFile("Assets/Graphics/coinBronze.png");
	itemTextures[1].loadFromFile("Assets/Graphics/coinSilver.png");
	itemTextures[2].loadFromFile("Assets/Graphics/coinGold.png");

	// Create the vector to hold our items
	std::vector<Item> items;
	// Load up a few starting items
	items.push_back(Item(itemTextures, gameWindow.getSize()));
	items.push_back(Item(itemTextures, gameWindow.getSize()));
	items.push_back(Item(itemTextures, gameWindow.getSize()));

	// Create a time value to store the total time between each item spawn
		sf::Time itemSpawnDuration = sf::seconds(2.0f);
	// Create a timer to store the time remaining for our game
	sf::Time itemSpawnRemaining = itemSpawnDuration;

	// Load the pickup sound effect file into a soundBuffer
	sf::SoundBuffer pickupSoundBuffer;
	pickupSoundBuffer.loadFromFile("Assets/Audio/pickup.wav");
	// Setup a Sound object to play the sound later, and associate it with the SoundBuffer
	sf::Sound pickupSound;
	pickupSound.setBuffer(pickupSoundBuffer);

	// Load the victory sound effect file into a soundBuffer
	sf::SoundBuffer victorySoundBuffer;
	victorySoundBuffer.loadFromFile("Assets/Audio/victory.ogg");
	// Setup a Sound object to play the sound later, and associate it with the SoundBuffer
	sf::Sound victorySound;
	victorySound.setBuffer(victorySoundBuffer);

	// Game over variable to track if the game is done
	bool gameOver = false;

	// Game Over Text
    // Declare a text variable called gameOverText to hold our game over display
	sf::Text gameOverText;
	// Set the font our text should use
	gameOverText.setFont(gameFont);
	// Set the string of text that will be displayed by this text object
	gameOverText.setString("GAME OVER PLEASE PRESS SPACEBAR TO TRYAGAIN ");
	// Set the size of our text, in pixels
	gameOverText.setCharacterSize(64);
	// Set the colour of our text
	gameOverText.setFillColor(sf::Color::Cyan);
	// Set the text style for the text
	gameOverText.setStyle(sf::Text::Bold | sf::Text::Italic);
	// Position our text in the top center of the screen
	gameOverText.setPosition(gameWindow.getSize().x / 2 - gameOverText.getLocalBounds().width / 2, 150);
	

																												 
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Game Loop
	while (gameWindow.isOpen()) 
	{
		//**************************************************** TODO: Check for input
		// Declare a variable to hold an Event, called gameEvent
		sf::Event gameEvent;
		// Loop through all events and poll them, putting each one into our gameEvent variable
			while (gameWindow.pollEvent(gameEvent))
			{
				// This section will be repeated for each event waiting to be processed
					// Did the player try to close the window?
					if (gameEvent.type == sf::Event::Closed)
					{
						// If so, call the close function on the window.
						gameWindow.close();
					}
			}
		// End event polling loop
		// Player keybind input
		playerObject.Input();


		//*************************************************** TODO: Update game state

		// Get the time passed since the last frame and restart our game clock
		sf::Time frameTime = gameClock.restart();

		// Update our time remaining based on how much time passed last frame
		timeRemaining = timeRemaining - frameTime;

		// Check if time has run out
		if (timeRemaining.asSeconds() <= 0)
		{
			// Don't let the time go lower than 0
			timeRemaining = sf::seconds(0);
			// Perform these actions only once when the game first ends:
			if (gameOver == false)
			{
				// Set our gameOver to true now so we don't perform these actions again
				gameOver = true;
				// Stop the main music from playing
				gameMusic.stop();
				// Play our victory sound
				victorySound.play();
			}
		}

		// Only perform this update logic if the game is still running:
		if (!gameOver)
		{

			// Update our time display based on our time remaining
			timerText.setString("Time Remaining: " + std::to_string((int)timeRemaining.asSeconds()));

			// Update our score display text based on our current numerical score
			scoreText.setString("Score: " + std::to_string(score));

			// Update our item spawn time remaining based on how much time passed last frame
			itemSpawnRemaining = itemSpawnRemaining - frameTime;

			// Check if time remaining to next spawn has reached 0

			if (itemSpawnRemaining <= sf::seconds(0.0f))
			{
				// Time to spawn a new item!
				items.push_back(Item(itemTextures, gameWindow.getSize()));

				// Reset time remaining to full duration
				itemSpawnRemaining = itemSpawnDuration;
			}
			// Move the player
			playerObject.Update(frameTime);

			// Check for collisions
			for (int i = 0; i < items.size(); ++i)
			{
				sf::FloatRect itemBounds = items[i].sprite.getGlobalBounds();
				sf::FloatRect playerBounds = playerObject.sprite.getGlobalBounds();
				if (itemBounds.intersects(playerBounds))
				{
					// Our player touched the item!
					// Add the item's value to the score
					score += items[i].pointsValue;
								  
					// incease speed movement
					playerObject.speed += 30.0f;
					
					// Play the pickup sound
					pickupSound.play();
					// Remove the item from the vector
					items.erase(items.begin() + i);
				}
			}
		}

		// Check if we should reset the game
		if (gameOver && sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
		{
			// Reset the game
			score = 0;
			timeRemaining = timeLimit;
			items.clear();
			items.push_back(Item(itemTextures, gameWindow.getSize()));
			items.push_back(Item(itemTextures, gameWindow.getSize()));
			items.push_back(Item(itemTextures, gameWindow.getSize()));
			gameMusic.play();
			gameOver = false;
		}

		

		//*************************************************** TODO: Draw graphics

		// Clear the window to a single colour
		gameWindow.clear(sf::Color::Black);
		// Draw everything to the window
		gameWindow.draw(playerObject.sprite);
		// Drawing Text to Screen
		gameWindow.draw(titleText);
		gameWindow.draw(authorText);
		gameWindow.draw(scoreText);
		gameWindow.draw(timerText);

		// Draw all our items if Game running
		if (!gameOver)
		{
			gameWindow.draw(playerObject.sprite);
			// Draw all our items
			for (int i = 0; i < items.size(); ++i)
			{
				gameWindow.draw(items[i].sprite);
			}
		}

		// Only draw these items if the game HAS ended:
		if (gameOver)
		{
			gameWindow.draw(gameOverText);
		}
		// Display the window contents on the screen
		gameWindow.display();


	}
	// End of Game Loop
	return 0;
}