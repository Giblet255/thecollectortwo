#include "Item.h"
#include <cstdlib>

Item::Item(std::vector<sf::Texture>& itemTextures, sf::Vector2u screenSize)
{
	int chosenIndex = rand() % itemTextures.size();
	sprite.setTexture(itemTextures[chosenIndex]);
	pointsValue = chosenIndex * 100 + 100;

	int positionX = rand() % (screenSize.x - itemTextures[chosenIndex].getSize().x);
	int positionY = rand() % (screenSize.y - itemTextures[chosenIndex].getSize().y);

	// Set our sprite position based on these random positions
	sprite.setPosition(positionX, positionY);
}
